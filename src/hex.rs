use bevy::math::Vec2;

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
pub struct Hex {
    q: i32,
    r: i32,
}

impl Hex {
    pub const DIRECTIONS: [Hex; 6] = [
        Hex::from_qrs(1, 0, -1),
        Hex::from_qrs(-1, 0, 1),
        Hex::from_qrs(1, -1, 0),
        Hex::from_qrs(-1, 1, 0),
        Hex::from_qrs(0, 1, -1),
        Hex::from_qrs(0, -1, 1),
    ];

    pub const ZERO: Hex = Hex::from_qr(0, 0);
}

impl Hex {
    pub const fn from_qr(q: i32, r: i32) -> Self {
        Hex { q, r }
    }

    pub const fn from_qrs(q: i32, r: i32, s: i32) -> Self {
        assert!(q + r + s == 0);
        Hex { q, r }
    }

    pub const fn from_odd_r(col: i32, row: i32) -> Self {
        Self::from_qr(col - (-row - (-row & 1)) / 2, -row)
    }

    pub const fn from_even_r(col: i32, row: i32) -> Self {
        Self::from_qr(col - (-row + (-row & 1)) / 2, -row)
    }

    pub const fn from_odd_q(col: i32, row: i32) -> Self {
        Self::from_qr(col, -row - (col - (col & 1)) / 2)
    }

    pub const fn from_even_q(col: i32, row: i32) -> Self {
        Self::from_qr(col, -row - (col + (col & 1)) / 2)
    }

    pub const fn q(&self) -> i32 {
        self.q
    }

    pub const fn r(&self) -> i32 {
        self.r
    }

    pub const fn s(&self) -> i32 {
        -self.q - self.r
    }

    pub fn neighbors(&self) -> [Hex; 6] {
        Self::DIRECTIONS.map(|d| *self + d)
    }
}

impl Hex {
    pub const fn length(&self) -> u32 {
        (self.q().abs() + self.r().abs() + self.s().abs()) as u32 / 2
    }

    pub const fn distance(&self, rhs: Self) -> u32 {
        // To have this function `const`, we cannot use `ops::Sub`
        Hex::from_qr(self.q() - rhs.q(), self.r() - rhs.r()).length()
    }
}

impl Hex {
    pub fn pointy_to_pixel(&self) -> Vec2 {
        Vec2::new(
            f32::sqrt(3.0) * self.q() as f32 + f32::sqrt(3.0) / 2.0 * self.r() as f32,
            -3.0 / 2.0 * self.r() as f32,
        )
    }

    pub fn flat_to_pixel(&self) -> Vec2 {
        Vec2::new(
            3.0 / 2.0 * self.q() as f32,
            -f32::sqrt(3.0) / 2.0 * self.q() as f32 - f32::sqrt(3.0) * self.r() as f32,
        )
    }

    pub fn from_pixel_pointy(px: Vec2) -> Self {
        Hex::from_qr_f32(
            1. / f32::sqrt(3.0) * px.x + 1. / 3. * px.y, //
            -2. / 3. * px.y,
        )
    }

    pub fn from_pixel_flat(px: Vec2) -> Self {
        Hex::from_qr_f32(
            2. / 3. * px.x, //
            -1. / 3. * px.x - 1. / f32::sqrt(3.0) * px.y,
        )
    }

    pub fn from_qrs_f32(q: f32, r: f32, s: f32) -> Self {
        let mut q_round = q.round();
        let mut r_round = r.round();
        let mut s_round = s.round();

        let q_diff = q_round - q;
        let r_diff = r_round - r;
        let s_diff = s_round - s;

        if q_diff > r_diff && q_diff > s_diff {
            q_round = -r_round - s_round;
        } else if r_diff > s_diff {
            r_round = -q_round - s_round;
        } else {
            s_round = -q_round - r_round;
        }

        Self::from_qrs(q_round as i32, r_round as i32, s_round as i32)
    }

    pub fn from_qr_f32(q: f32, r: f32) -> Self {
        Self::from_qrs_f32(q, r, -q - r)
    }
}

impl std::ops::Add for Hex {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Hex::from_qr(self.q() + rhs.q(), self.r() + rhs.r())
    }
}

impl std::ops::AddAssign for Hex {
    fn add_assign(&mut self, rhs: Self) {
        *self = *self + rhs
    }
}

impl std::ops::Sub for Hex {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Hex::from_qr(self.q() - rhs.q(), self.r() - rhs.r())
    }
}

impl std::ops::SubAssign for Hex {
    fn sub_assign(&mut self, rhs: Self) {
        *self = *self - rhs
    }
}

impl std::ops::Mul<i32> for Hex {
    type Output = Hex;

    fn mul(self, rhs: i32) -> Self::Output {
        Hex::from_qr(self.q() * rhs, self.r() * rhs)
    }
}

impl std::ops::Mul<Hex> for i32 {
    type Output = Hex;

    fn mul(self, rhs: Hex) -> Self::Output {
        Hex::from_qr(self * rhs.q(), self * rhs.r())
    }
}

// impl std::ops::Div<i32> for Hex {
//     type Output = Hex;

//     fn div(self, rhs: i32) -> Self::Output {
//         Hex::from_qr_f32(self.q() as f32 / rhs as f32, self.r() as f32 / rhs as f32)
//         // Hex::from_qr(self.q() / rhs, self.r() / rhs)
//     }
// }
