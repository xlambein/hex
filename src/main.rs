use bevy::prelude::*;
use bevy_prototype_lyon::prelude::*;

mod hex;

use hex::Hex;

#[derive(Debug, Default, Clone, Copy, Component)]
struct HexPosition(Hex);

#[derive(Debug, Default, Clone, Copy, Component)]
struct HexVelocity(Hex);

#[derive(Component)]
struct Tween;

struct Step;

const RADIUS: f32 = 20.0;

fn main() {
    App::new()
        .insert_resource(Msaa { samples: 4 })
        .add_plugins(DefaultPlugins)
        .add_plugin(ShapePlugin)
        .add_event::<Step>()
        .add_startup_system(setup)
        .add_system_to_stage(CoreStage::Update, hex_world_sync)
        .add_system_to_stage(CoreStage::Update, vel_rot_sync)
        .add_system_to_stage(CoreStage::PreUpdate, hex_move)
        .add_system(keyboard_input)
        .run();
}

fn hex_to_world(hex: IVec2) -> Vec2 {
    let sin_tau_12: f32 = f32::sin(std::f32::consts::TAU / 12.0);
    let cos_tau_12: f32 = f32::cos(std::f32::consts::TAU / 12.0);

    let [x, y] = hex.to_array();
    Vec2::new(
        x as f32 * (1. + sin_tau_12),
        (y as f32 + if x % 2 == 0 { 0.0 } else { 0.5 }) * 2. * cos_tau_12,
    )
}

fn gravity_at(hex: Hex) -> Hex {
    match hex.length() {
        1 => -4 * hex,
        2 => -1 * hex,
        3 => {
            -1 * Hex::from_qrs_f32(
                hex.q() as f32 / 3.,
                hex.r() as f32 / 3.,
                hex.s() as f32 / 3.,
            )
        }
        _ => Hex::ZERO,
    }
}

fn spawn_hexes(commands: &mut Commands, asset_server: &Res<AssetServer>) {
    let shape = shapes::RegularPolygon {
        sides: 6,
        feature: shapes::RegularPolygonFeature::Radius(RADIUS),
        ..shapes::RegularPolygon::default()
    };

    let q_vec = Vec2::new(0.0, 1.0);
    let r_vec = Vec2::new(f32::sqrt(3.0) / 2.0, -0.5);
    let s_vec = Vec2::new(-f32::sqrt(3.0) / 2.0, -0.5);

    let q_color = Color::hsla(90.0, 1.0, 0.35, 0.3);
    let r_color = Color::hsla(200.0, 1.0, 0.45, 0.3);
    let s_color = Color::hsla(300.0, 0.8, 0.5, 0.3);

    let font = asset_server.load("FiraSans-Bold.ttf");
    let text_style = TextStyle {
        font: font.clone(),
        font_size: 10.0,
        color: Color::GRAY,
    };
    let text_alignment = TextAlignment {
        vertical: VerticalAlign::Center,
        horizontal: HorizontalAlign::Center,
    };

    fn hex_text(
        text: impl Into<String>,
        font: &Handle<Font>,
        color: Color,
        translation: Vec2,
    ) -> impl Bundle {
        let text_style = TextStyle {
            font: font.to_owned(),
            font_size: 10.0,
            color,
        };
        let text_alignment = TextAlignment {
            vertical: VerticalAlign::Center,
            horizontal: HorizontalAlign::Center,
        };
        Text2dBundle {
            text: Text::with_section(text, text_style, text_alignment),
            transform: Transform::from_translation((translation * RADIUS).extend(0.5)),
            ..Default::default()
        }
    }

    for x in -20..=20 {
        for y in -20..=20 {
            let hex = Hex::from_odd_q(x, y);
            let g = gravity_at(hex);
            commands
                .spawn_bundle(GeometryBuilder::build_as(
                    &shape,
                    DrawMode::Outlined {
                        fill_mode: FillMode::color(Color::BLACK),
                        outline_mode: StrokeMode::new(Color::DARK_GRAY, RADIUS * 0.1),
                    },
                    Transform::default(),
                ))
                .insert(HexPosition(hex))
                .with_children(|commands| {
                    // commands.spawn_bundle(Text2dBundle {
                    //     text: Text::with_section(
                    //         format!("{}", g.length()),
                    //         text_style.clone(),
                    //         text_alignment,
                    //     ),
                    //     transform: Transform::from_xyz(0., 0., 0.5),
                    //     ..Default::default()
                    // });
                    // commands.spawn_bundle(hex_text(
                    //     format!("{}", hex.q()),
                    //     &font,
                    //     q_color,
                    //     q_vec * 0.6,
                    // ));
                    // commands.spawn_bundle(hex_text(
                    //     format!("{}", hex.r()),
                    //     &font,
                    //     r_color,
                    //     r_vec * 0.6,
                    // ));
                    // commands.spawn_bundle(hex_text(
                    //     format!("{}", hex.s()),
                    //     &font,
                    //     s_color,
                    //     s_vec * 0.6,
                    // ));
                    commands.spawn_bundle(GeometryBuilder::build_as(
                        &shapes::Line(Vec2::ZERO, g.flat_to_pixel() * RADIUS / 8.),
                        DrawMode::Outlined {
                            fill_mode: FillMode::color(Color::RED),
                            outline_mode: StrokeMode::new(Color::RED, RADIUS * 0.1),
                        },
                        Transform::from_xyz(0., 0., 0.5),
                    ));
                });
        }
    }
}

fn hex_world_sync(mut query: Query<(&HexPosition, &mut Transform, Option<&Tween>)>) {
    for (pos, mut t, tween) in query.iter_mut() {
        let speed = 1000.0;
        let fps = 60.0;
        let current = t.translation.truncate();
        let target = pos.0.flat_to_pixel() * RADIUS;
        let diff = target - current;
        let dist = diff.length();
        if dist > 1e-3 {
            let dir = diff / dist;
            let update = if tween.is_some() {
                current + dir * f32::min(speed / fps, dist)
            } else {
                target
            };

            t.translation = update.extend(t.translation.z);
        }
    }
}

fn vel_rot_sync(mut query: Query<(&HexVelocity, &mut Transform)>) {
    for (vel, mut t) in query.iter_mut() {
        t.rotation = Quat::from_rotation_z(Vec2::X.angle_between(vel.0.flat_to_pixel()));
    }
}

fn hex_move(mut query: Query<(&mut HexPosition, &mut HexVelocity)>, mut events: EventReader<Step>) {
    for _ in events.iter() {
        for (mut pos, mut vel) in query.iter_mut() {
            let g = gravity_at(pos.0);
            vel.0 += g;
            pos.0 += vel.0;
        }
    }
}

// fn gravity(mut query: Query<(&mut HexVelocity, &HexPosition)>) {
//     for (mut vel, pos) in query.iter_mut() {
//         vel += gravity_at(pos);
//     }
// }

fn keyboard_input(input: Res<Input<KeyCode>>, mut events: EventWriter<Step>) {
    if input.just_pressed(KeyCode::Space) {
        events.send(Step);
    }
}

fn spawn_boid(commands: &mut Commands, pos: Hex, vel: Hex, color: Color) {
    let shape = shapes::Polygon {
        points: vec![
            Vec2::new(1.0, 0.0),
            Vec2::new(-1.0, 0.5),
            Vec2::new(-0.5, 0.0),
            Vec2::new(-1.0, -0.5),
        ]
        .into_iter()
        .map(|v| v * RADIUS * 0.5)
        .collect(),
        closed: true,
    };
    commands
        .spawn_bundle(GeometryBuilder::build_as(
            &shape,
            DrawMode::Outlined {
                fill_mode: FillMode::color(color),
                outline_mode: StrokeMode::new(Color::GRAY, RADIUS * 0.1),
            },
            Transform::from_xyz(0., 0., 1.0),
        ))
        .insert(HexPosition(pos))
        // .insert(HexPosition(Hex::from_qr(2, 2)))
        .insert(HexVelocity(vel))
        .insert(Tween);
}

fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
    spawn_hexes(&mut commands, &asset_server);

    let jupiter_tex = asset_server.load("jupiter.png");

    commands.spawn_bundle(SpriteBundle {
        sprite: Sprite {
            custom_size: Some(Vec2::splat(RADIUS * 1.6)),
            ..Default::default()
        },
        transform: Transform::from_translation(Vec3::Z * 0.1),
        texture: jupiter_tex,
        ..Default::default()
    });

    spawn_boid(
        &mut commands,
        Hex::from_qr(3, 1),
        Hex::from_qr(0, -1),
        Color::hsl(120.0, 0.8, 0.5),
    );
    spawn_boid(
        &mut commands,
        Hex::from_qr(1, 3),
        Hex::from_qr(0, -1),
        Color::hsl(0.0, 0.8, 0.5),
    );
    spawn_boid(
        &mut commands,
        Hex::from_qr(2, 2),
        Hex::from_qr(0, -1),
        Color::hsl(120.0, 0.8, 0.5),
    );
    spawn_boid(
        &mut commands,
        Hex::from_qr(0, 4),
        Hex::from_qr(0, -1),
        Color::hsl(240.0, 0.8, 0.5),
    );
}
