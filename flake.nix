{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, fenix }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        toolchain = fenix.packages.${system}.fromToolchainFile {
          file = ./rust-toolchain.toml;
          sha256 = "KK/6waHKujphwgbEMhw/jmCorSm+Xt4DxKm1skj1RZE=";
        };
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            fenix.overlay # for rust-analyzer-nightly
            (self: super: {
              rustc = toolchain;
            })
          ];
        };
      in
      rec {
        devShell = with pkgs; mkShell {
          buildInputs = [
            cargo
            rustc
            rustfmt
            pre-commit
            rustPackages.clippy
            crate2nix
            rust-analyzer-nightly
            cargo-edit

            llvmPackages.bintools
            clang
            pkgconfig
            udev
            alsaLib
            xlibsWrapper
            xorg.libXcursor
            xorg.libXrandr
            xorg.libXi
            vulkan-tools
            vulkan-headers
            vulkan-loader
            vulkan-validation-layers
          ];
          RUST_SRC_PATH = rustPlatform.rustLibSrc;
          shellHook = ''export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${pkgs.lib.makeLibraryPath [
            pkgs.alsaLib
            pkgs.udev
            pkgs.vulkan-loader
          ]}"'';
        };

      });
}
